package lk.dialog.training.software.development.project;

public class Calculator {

    /**
     *  Adds two int values
     *
     * @param a value in int
     * @param b value in int
     * @return addition of a and b in int
     */

    public static int add(int a, int b) {
        return a + b;
    }

    /**
     *  Subracts two int values
     *
     * @param a value in int
     * @param b value in int
     * @return subtraction of a and b in int
     */
    @Deprecated
    public static int sub(int a, int b) {
        return a - b;
    }
}
