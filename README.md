# SonarLint & SonarQube

This repository contains all the prerequisites required for the SonarLint and SonarQube session of the workshop. Please complete below steps before the session.

## 1. Install IntellJ IDEA Community Edition

[Download](https://www.jetbrains.com/idea/download/download-thanks.html?platform=windows&code=IIC), install and run IntellJ IDEA Community Edition.

## 2. Download Repository

Download this repository to the working directory.
```shell  
git clone https://gitlab.com/dumindurr/software-development-project.git 
```

## 3. Open Project

Open the project by going to **File** -> **Open** and select the location of the repository in the working directory.

## 4. Install SonarLint Plugin

Install the SonarLint plugin for IntelliJ by going to **File** -> **Settings**. In the Settings window click on **Plugins**, search the marketplace for **SonarLint** and click **Install** to install the plugin to the IDE.

## Sonar Test

```shell
mvn sonar:sonar  -Dsonar.projectKey=development-training-project -Dsonar.host.url=http://35.222.165.252 -Dsonar.login=e37183280d7ad17cf05c2982afd75ed5158f9bfa
  ```